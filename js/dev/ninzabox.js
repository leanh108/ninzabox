var ninzabox = new function () {
    this.alert = function (message) {
        $("#ninza-overlay").show();
        $("#ninza-alert").show();
        $("#ninza-alert .ninza-message").html(message);
    }

    this.success = function (message) {
        $("#ninza-overlay").show();
        $("#ninza-success").show();
        $("#ninza-success .ninza-message").html(message);
    }

    this.info = function (message) {
        $("#ninza-overlay").show();
        $("#ninza-info").show();
        $("#ninza-info .ninza-message").html(message);
    }

    this.closeBox = function (boxId) {
        $("#ninza-overlay").hide();
        $("#" + boxId).hide();
        $("#" + boxId + " .ninza-message").html("");
    }

    this.confirm = function (message, yesCallback, noCallback) {
        $("#ninza-confirm #btnYes").unbind("click");
        $("#ninza-confirm #btnNo").unbind("click");
        var overlay = $("#ninza-overlay");
        var confirmBox = $("#ninza-confirm");
        overlay.show();
        confirmBox.show();
        confirmBox.find(".ninza-message").html(message);
        $("#ninza-confirm #btnYes").bind("click", function () {
            overlay.hide();
            confirmBox.hide();
            if (yesCallback != null && typeof (yesCallback) != 'undefined') {
                yesCallback();
            }
        });
        $('#ninza-confirm #btnNo').bind("click", function () {
            overlay.hide();
            confirmBox.hide();
            if (noCallback != null && typeof (noCallback) != 'undefined') {
                noCallback();
            }
        });
    }
}

var boxGenerator = new function () {

    this.overlay = function () {
        var ninzaOverlay = "<div id='ninza-overlay'></div>";

        if ($("#ninza-overlay").length <= 0) {
            $("body").append(ninzaOverlay);
        }
    }

    //generate ra những box chỉ có 1 nút
    this.createSingleButtonBox = function (boxIdentity, btnClass) {
        if ($("#ninza-" + boxIdentity).length <= 0) {
            var overlay = $("#ninza-overlay");
            overlay.append("<div id='ninza-" + boxIdentity + "' class='ninza-box'></div>");
            overlay.find("#ninza-" + boxIdentity).append("<div class='ninza-box-icon'></div>");
            overlay.find("#ninza-" + boxIdentity).append("<p class='ninza-message'></p>");
            overlay.find("#ninza-" + boxIdentity).append("<div class='text-center'><input type='button' class='ninza-btn "+btnClass+"' value='OK'/></div>");
            overlay.find("#ninza-" + boxIdentity).find(".ninza-btn").bind("click", function () {
                ninzabox.closeBox("ninza-" + boxIdentity);
            });
        }
    }

    this.alert = function () {
        boxGenerator.createSingleButtonBox('alert','ninza-btn-danger'); 
    };

    this.success = function(){
        boxGenerator.createSingleButtonBox('success','ninza-btn-success');
    }

    this.info = function(){
        boxGenerator.createSingleButtonBox('info','ninza-btn-info');
    }

    this.confirm = function(){
        if ($("#ninza-confirm").length <= 0) {
            var overlay = $("#ninza-overlay");
            overlay.append("<div id='ninza-confirm' class='ninza-box'></div>");
            overlay.find("#ninza-confirm").append("<div class='ninza-box-icon'></div>");
            overlay.find("#ninza-confirm").append("<p class='ninza-message'></p>");
            overlay.find("#ninza-confirm").append("<div class='text-center'>"+
                                                    "<input type='button' id='btnYes' class='ninza-btn ninza-btn-success' value='CÓ'/>"+
                                                    "<input type='button' id='btnNo' class='ninza-btn ninza-btn-danger' value='KHÔNG'/>"+
                                                "</div>");
           
        }
    }

    this.init = function () {
        boxGenerator.overlay();
        boxGenerator.alert();
        boxGenerator.success();
        boxGenerator.info();
        boxGenerator.confirm();
    }
}

$(document).ready(function () {
    boxGenerator.init();
});